icnsify(1) -- Easily create .icns files (Mac Icons).
=============================================

## SYNOPSIS

`icnsify` [options]

## DESCRIPTION

A small CLI app icnsify is provided allowing you to create icns files
using this library from the command line. It supports piping, which is
something iconutil does not do, making it substantially easier to wrap
or chuck into a shell pipeline.

## OPTIONS

 * `-i`, `--input` *string* :
  Input image for conversion to icns from jpg|png or visa versa.
  
 * `-o`, `--output` *string*:
  Output path, defaults to <path/to/image>.(icns|png) depending on input.
   
 * `-r`, `--resize` *int*:
  Quality of resize algorithm. Values range from 0 to 5, fastest to slowest execution time. Defaults to slowest for best quality. (default 5)
